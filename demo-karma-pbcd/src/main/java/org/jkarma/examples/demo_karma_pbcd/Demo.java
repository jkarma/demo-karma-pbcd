/*******************************************************************************
 * Copyright 2019 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.examples.demo_karma_pbcd;

import java.util.stream.Stream;

import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.windows.Windows;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.TemporalGraph;
import org.jkarma.pbcd.descriptors.Descriptors;
import org.jkarma.pbcd.detectors.Detectors;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.events.ChangeDescriptionCompletedEvent;
import org.jkarma.pbcd.events.ChangeDescriptionStartedEvent;
import org.jkarma.pbcd.events.ChangeDetectedEvent;
import org.jkarma.pbcd.events.ChangeNotDetectedEvent;
import org.jkarma.pbcd.events.PBCDEventListener;
import org.jkarma.pbcd.events.PatternUpdateCompletedEvent;
import org.jkarma.pbcd.events.PatternUpdateStartedEvent;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.pbcd.similarities.UnweightedJaccard;


/**
 * A simple class serving as the entry point for the illustrative 
 * example of a project template which uses jKarma.
 * In this class we first define a custom PBCD based on frequent 
 * connected subgraphs, and then we illustrate how it can be 
 * executed on simple datasets.
 * 
 * @author Angelo Impedovo
 */
public class Demo{
	
	/**
	 * The block size indicating that the change is measured every blockSize graphs.
	 */
	public int blockSize = 10;
	
	/**
	 * The maximum size (edges) of frequent connected sub-graphs.
	 */
	public int maxDepth = 3;
	
	/**
	 * The minimum support used by the eclat algorithm.
	 */
	public float minSup = 0.15f;
	
	/**
	 * Minimum change threshold. Change scores exceeding this value are considered changes.
	 */
	public float minChange = 0.25f; 
	
	/**
	 * Minimum growth-rate threshold. Used to discover emerging patterns characterizing any
	 * detected change.
	 */
	public float minGr = 1.0f;
	
	
	/**
	 * Builds a PBCD based on the KARMA algorithm.
	 * 
	 * @return a PBCD implementing the KARMA algorithm
	 * 
	 * @see <a href="https://doi.org/10.1016/j.knosys.2018.07.0110">Knowledge-based Systems, 
	 * 2018, Mining Macroscopic and Microscopic Changes on Network Data Streams</a>
	 * @see <a href="http://ceur-ws.org/Vol-2400/paper-23.pdf">Proceedings of SEBD 2019, 2019, 
	 * Mining Macroscopic and Microscopic Changes in Network Data Streams (Discussion Paper)</a>
	 */
	public PBCD<TemporalGraph, LabeledEdge, TidSet, Boolean> buildDetector(){
		//a data accessor is built
		TidSetProvider<LabeledEdge> accessor = new TidSetProvider<>(Windows.cumulativeLandmark());
		
		//a frequent connected subgraph mining based on the eclat algorithm is defined
		MiningStrategy<LabeledEdge, TidSet> strategy = Strategies
			.uponSubgraphs()
			.limitDepth(maxDepth)
			.eclat(minSup)
			.dfs(accessor);
		
		//then the karma detector is built and returned
		UnweightedJaccard distanceMeasure = new UnweightedJaccard();
		return Detectors.upon(strategy)
			.unweighted((p,s) -> Patterns.isFrequent(p, minSup, s), distanceMeasure)
			.describe(Descriptors.partialEps(minSup, minGr))
			.build(minChange, blockSize);
	}
	
	
	/**
	 * Convenience method returning an empty stream of time-stamped graphs, 
	 * the stream should be temporally sorted.
	 * The user should alter the content of this method to load and return 
	 * the desired dataset.
	 * 
	 * @return a Stream of TemporalGraph objects
	 */
	public Stream<TemporalGraph> getDataset(){
		return Stream.empty();
	}
	
	
	/**
	 * Entry point of the template project. The application load a dataset,
	 * then builds a PBCD based on the KARMA algorithm, and use this pbcd to
	 * consume the dataset in search of changes.
	 * The application reacts to change detection event by listening to them
	 * in the CustomEventListener component.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Demo app = new Demo();
		
		//a dummy dataset is loaded
		Stream<TemporalGraph> dataset = app.getDataset();
		
		//the KARMA PBCD is built
		PBCD<TemporalGraph, LabeledEdge, TidSet, Boolean> detector = app.buildDetector();
		
		//listen to some change detection related events
		detector.registerListener(new PBCDEventListener<LabeledEdge, TidSet>(){

			@Override
			public void changeDescriptionCompleted(ChangeDescriptionCompletedEvent<LabeledEdge, TidSet> event) {
				//do nothing here
			}

			@Override
			public void changeDescriptionStarted(ChangeDescriptionStartedEvent<LabeledEdge, TidSet> event) {
				//do nothing here
			}

			@Override
			public void changeDetected(ChangeDetectedEvent<LabeledEdge, TidSet> event) {
				// reaction in case of change detected
				double changeScore = event.getAmount();
				System.out.println(changeScore);
			}

			@Override
			public void changeNotDetected(ChangeNotDetectedEvent<LabeledEdge, TidSet> event) {
				// reaction in case of change not detected
				double changeScore = event.getAmount();
				System.out.println(changeScore);
			}

			@Override
			public void patternUpdateCompleted(PatternUpdateCompletedEvent<LabeledEdge, TidSet> event) {
				//do nothing here
			}

			@Override
			public void patternUpdateStarted(PatternUpdateStartedEvent<LabeledEdge, TidSet> event) {
				//do nothing here
			}
			
		});
		
		//the PBCD consume the dataset in search of changes
		dataset.forEach(detector);
	}
}
