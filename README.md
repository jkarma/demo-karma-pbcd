# demo-karma-pbcd

Demo project using jKarma for detecting changes on a dynamic network represented as a stream of graph snapshots. 
The problem is solved by implementing the KARMA PBCD algorithm with jKarma.